# Aller Site Dashboard

A dashboard listing all Aller sites that detects slowly loading pages.

##### Features:

  - All aller sites listed in mobile views
  - Detects slow page loads

##### Installation:

Clone the GIT repository like this:

```sh
cd /the/location/of/your/choice
git clone https://thomasdjupsjo@bitbucket.org/allermedia/fi-utilities-aller-site-dashboard.git
```

##### License & Author
Aller Media Oy &copy;
Thomas Djupsjö
