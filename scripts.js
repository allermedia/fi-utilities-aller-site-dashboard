//Execute code on load
window.onload = function () {
//Count total loading time
  var loadingStart = window.performance.timing.navigationStart;
  var loadingEnd =  window.performance.timing.domComplete;
  var loadingTime  = ((loadingEnd-loadingStart)/1000).toFixed(2);
  document.getElementById('loadingTime').style.display = 'block';
  document.getElementById('loadingTime').innerHTML = 'Total loading time for the sites <b>'+loadingTime+ ' </b>seconds';
  //Show slow loading time
  if ( loadingTime > 40 ) {
    document.getElementById('loadingTimeSlow').style.display = 'block';
  }
}
//Hard refresh of site once in five minutes
setInterval(function(){
  window.location.reload(true);
}, 300000);
